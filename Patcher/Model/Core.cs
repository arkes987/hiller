﻿using MemoryScanner;
using MemoryScanner.Addresses;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patcher.Model
{
    public class Core : GetAddresses
    {
        #region .ctor

        public Core() { }

        public Core(Process p)
        {
            this.p = p;
            memScan = new MemoryScanner.MemoryScanner(p);
            memRead = new MemoryReader(p);
            list = new List<MemoryScanner.Addresses.GetAddresses>();
            MemoryScanner.Util.GlobalVars.FullPath = p.Modules[0].FileName;
            InitAddresses();
            list.Clear();
            StartSearch();
        }

        #endregion

        MemoryScanner.MemoryScanner memScan;
        MemoryReader memRead;
        Process p;
        List<MemoryScanner.Addresses.GetAddresses> list;

        public void InitAddresses()
        {
            MemoryScanner.Addresses.MyAddresses.Health = new MemoryScanner.Addresses.Health(memRead, memScan, MemoryScanner.Addresses.GetAddresses.AddressType.Player);
            MemoryScanner.Addresses.MyAddresses.XorKey = new MemoryScanner.Addresses.XorKey(memRead, memScan, MemoryScanner.Addresses.GetAddresses.AddressType.Player);
            MemoryScanner.Addresses.MyAddresses.Mana = new MemoryScanner.Addresses.Mana(memRead, memScan, MemoryScanner.Addresses.GetAddresses.AddressType.Player);
            MemoryScanner.Addresses.MyAddresses.PlayerId = new MemoryScanner.Addresses.PlayerId(memRead, memScan, MemoryScanner.Addresses.GetAddresses.AddressType.Player);
            MemoryScanner.Addresses.MyAddresses.PlayerZ = new MemoryScanner.Addresses.PlayerZ(memRead, memScan, MemoryScanner.Addresses.GetAddresses.AddressType.Player);
            MemoryScanner.Addresses.MyAddresses.Status = new MemoryScanner.Addresses.Status(memRead, memScan, MemoryScanner.Addresses.GetAddresses.AddressType.Client);
            MemoryScanner.Addresses.MyAddresses.BlistStart = new MemoryScanner.Addresses.BlistStart(memRead, memScan, MemoryScanner.Addresses.GetAddresses.AddressType.BattleList);
            MemoryScanner.Addresses.MyAddresses.BlistStep = new MemoryScanner.Addresses.BlistStep(memRead, memScan, MemoryScanner.Addresses.GetAddresses.AddressType.BattleList);
            MemoryScanner.Addresses.MyAddresses.MaxCreatures = new MemoryScanner.Addresses.MaxCreatures(memRead, memScan, MemoryScanner.Addresses.GetAddresses.AddressType.BattleList);
        }

        public void StartSearch()
        {
            Data.HealthAddress = MemoryScanner.Addresses.MyAddresses.Health.GetAddress();
            Data.XorAddress = MemoryScanner.Addresses.MyAddresses.XorKey.GetAddress();
            Data.ManaAddress = MemoryScanner.Addresses.MyAddresses.Mana.GetAddress();
            Data.PlayerId = MemoryScanner.Addresses.MyAddresses.PlayerId.GetAddress();
            Data.PositionZAddress = MemoryScanner.Addresses.MyAddresses.PlayerZ.GetAddress();
            Data.IsConnectedAddress = MemoryScanner.Addresses.MyAddresses.Status.GetAddress();
            Data.BattleListStart = MemoryScanner.Addresses.MyAddresses.BlistStart.GetAddress();
            Data.BattleListStepCreatures = MemoryScanner.Addresses.MyAddresses.BlistStep.GetAddress();
            Data.BattleListMaxCreatures = MemoryScanner.Addresses.MyAddresses.MaxCreatures.GetAddress();
            Data.BattleListEnd = Data.BattleListStart + (Data.BattleListStepCreatures * Data.BattleListMaxCreatures);
        }

    }
}
