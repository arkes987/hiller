﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MemoryScanner.Addresses
{
    public static class MyAddresses
    {

        public static Addresses.Health Health;
        public static Addresses.XorKey XorKey;
        public static Addresses.Mana Mana;
        public static Addresses.PlayerId PlayerId;
        public static Addresses.PlayerZ PlayerZ;
        public static Addresses.Status Status;
        public static Addresses.BlistStart BlistStart;
        public static Addresses.BlistStep BlistStep;
        public static Addresses.MaxCreatures MaxCreatures;
    }
}
