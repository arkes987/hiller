﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patcher.Model
{
    public class Data
    {
        #region Properties

        public static int PlayerId { get; set; }
        public static int XorAddress { get; set; }
        public static int HealthAddress { get; set; }
        public static int ManaAddress { get; set; }
        public static int IsConnectedAddress { get; set; }
        public static int PositionZAddress { get; set; }

        public static int BattleListStart { get; set; }
        public static int BattleListStepCreatures { get; set; }
        public static int BattleListMaxCreatures { get; set; }
        public static int BattleListEnd { get; set; }

        public static int HotkeyTextStart { get; set; }
        public static int HotkeyObjectStart { get; set; }
        public static int HotkeyTextStep { get; set; }
        public static int HotkeyObjectStep { get; set; }

        public static int CreatureDistanceId { get; set; }
        public static int CreatureDistanceType { get; set; }
        public static int CreatureDistanceName { get; set; }

        public static int CreatureDistanceX { get; set; }
        public static int CreatureDistanceY { get; set; }
        public static int CreatureDistanceZ { get; set; }

        #endregion
    }
}
