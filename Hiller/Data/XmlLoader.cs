﻿using Hiller.Model;
using Hiller.Model.SubModels;
using Hiller.ViewModel.Libs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Hiller.Data
{
    public class XmlLoader : GxNotifyObject
    {
        #region .ctor

        public XmlLoader()
        {
            XmlReader = new XmlDocument();
            XmlReader.Load("Data\\Spells.xml");
            SpellsCollection = new ObservableCollection<Spell>();

        }

        #endregion

        public ObservableCollection<Spell> SpellsCollection { get; set; }


        private XmlDocument _xmlReader;
        public XmlDocument XmlReader
        {
            get { return _xmlReader; }
            set
            {
                if (_xmlReader != value)
                {
                    _xmlReader = value;
                    OnPropertyChanged(() => XmlReader);
                }
            }
        }

        private Spell _spell;
        public Spell Spell
        {
            get { return _spell; }
            set
            {
                if (_spell != value)
                {
                    _spell = value;
                    OnPropertyChanged(() => Spell);
                }
            }
        }

        public ObservableCollection<Spell> ReadSpells()
        {
            XmlNodeList xnList = XmlReader.SelectNodes("/Spells/Spell");
            foreach (XmlNode xn in xnList)
            {
                Spell spell = new Spell();
                spell.SpellName = xn["Name"].InnerText;
                spell.SpellImage = xn["ImageSource"].InnerText;
                SpellsCollection.Add(spell);
            }
            OutputManager.WriteCL("Spells loaded from source [DONE]","done");
            return SpellsCollection;
        }
    }
}
