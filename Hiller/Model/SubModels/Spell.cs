﻿using Hiller.ViewModel.Libs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hiller.Model.SubModels
{
    public class Spell : GxNotifyObject
    {
        private string _spellName;
        public string SpellName
        {
            get { return _spellName; }
            set
            {
                if (_spellName != value)
                {
                    _spellName = value;
                    OnPropertyChanged(() => SpellName);
                }
            }
        }

        private string _spellImage;
        public string SpellImage
        {
            get { return _spellImage; }
            set
            {
                if (_spellImage != value)
                {
                    _spellImage = value;
                    OnPropertyChanged(() => SpellImage);
                }
            }
        }
    }
}
