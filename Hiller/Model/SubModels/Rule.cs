﻿using Hiller.ViewModel.Libs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hiller.Model.SubModels
{
    public class Rule : GxNotifyObject
    {
        private string _item;
        public string Item
        {
            get { return _item; }
            set
            {
                if (_item != value)
                {
                    _item = value;
                    OnPropertyChanged(() => Item);
                }
            }
        }

        private string _healthType;
        public string HealthType
        {
            get { return _healthType; }
            set
            {
                if (_healthType != value)
                {
                    _healthType = value;
                    OnPropertyChanged(() => HealthType);
                }
            }
        }

        private string _manaType;
        public string ManaType
        {
            get { return _manaType; }
            set
            {
                if (_manaType != value)
                {
                    _manaType = value;
                    OnPropertyChanged(() => ManaType);
                }
            }
        }

        private int _priority;
        public int Priority
        {
            get { return _priority; }
            set
            {
                if (_priority != value)
                {
                    _priority = value;
                    OnPropertyChanged(() => Priority);
                }
            }
        }

        private int _minHp;
        public int MinHp
        {
            get { return _minHp; }
            set
            {
                if (_minHp != value)
                {
                    _minHp = value;
                    OnPropertyChanged(() => MinHp);
                }
            }
        }

        private int _minMp;
        public int MinMp
        {
            get { return _minMp; }
            set
            {
                if (_minMp != value)
                {
                    _minMp = value;
                    OnPropertyChanged(() => MinMp);
                }
            }
        }

        private int _minDelay;
        public int MinDelay
        {
            get { return _minDelay; }
            set
            {
                if (_minDelay != value)
                {
                    _minDelay = value;
                    OnPropertyChanged(() => MinDelay);
                }
            }
        }

        private int _maxDelay;
        public int MaxDelay
        {
            get { return _maxDelay; }
            set
            {
                if (_maxDelay != value)
                {
                    _maxDelay = value;
                    OnPropertyChanged(() => MaxDelay);
                }
            }
        }
    }
}
