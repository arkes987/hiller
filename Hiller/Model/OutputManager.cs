﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hiller.Model
{
    public class OutputManager
    {
        public static void WriteCL<T>(T message, string messageType)
        {
            switch(messageType)
            {
                case "log":
                    Console.ForegroundColor = ConsoleColor.Blue;
                    break;
                case "warn":
                    Console.ForegroundColor = ConsoleColor.Red;
                    break;
                case "done":
                    Console.ForegroundColor = ConsoleColor.Green;
                    break;
            }
            Console.WriteLine(message.ToString());
            Console.ResetColor();
        }

        public static void WriteCL(Exception exc)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(exc.ToString());
            Console.ResetColor();
        }

    }
}
