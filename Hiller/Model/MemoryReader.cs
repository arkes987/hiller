﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Patcher.Model;
using System.Windows;


namespace Hiller.Model
{
    public class MemoryReader
    {

        #region Properties

        public static int Id
        {
            get
            {
                return MemoryProvider.ReadInt32(Model.Character.Client.Handle, Patcher.Model.Data.PlayerId);
            }
        }

        public static int PositionZ
        {
            get
            {
                return MemoryProvider.ReadInt32(Model.Character.Client.Handle, Patcher.Model.Data.PositionZAddress);
            }
        }

        public static int Xor
        {
            get
            {
                return MemoryProvider.ReadInt32(Model.Character.Client.Handle, Patcher.Model.Data.XorAddress);
            }
        }


        public static int HealthPC
        {
            get
            {
                return (int)((double)Health / (double)HealthMax * 100);
            }
        }

        public static int ManaPC
        {
            get
            {
                double Mp = (double)Mana / (double)ManaMax * 100;
                if ((int)Mp < 1) 
                    return 1;
                return (int)Mp;
            }
        }

        public static int Health
        {
            get
            {
                return (MemoryProvider.ReadInt32(Model.Character.Client.Handle, Patcher.Model.Data.HealthAddress) ^ Xor);
            }
        }

        public static int HealthMax
        {
            get
            {
                return (MemoryProvider.ReadInt32(Model.Character.Client.Handle, Patcher.Model.Data.HealthAddress) ^ Xor);
            }
        }

        public static int ManaMax
        {
            get
            {
                return (MemoryProvider.ReadInt32(Model.Character.Client.Handle, Patcher.Model.Data.ManaAddress) ^ Xor);
            }
        }

        public static int Mana
        {
            get
            {
                return (MemoryProvider.ReadInt32(Model.Character.Client.Handle, Patcher.Model.Data.ManaAddress) ^ Xor);
            }
        }

        public static bool Connected
        {
            get
            {
                int status = MemoryProvider.ReadInt32(Model.Character.Client.Handle, Patcher.Model.Data.IsConnectedAddress);
                return (status == 11) ? true : false;
            }
        }
        #endregion
    }
}
