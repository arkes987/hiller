﻿using Hiller.Helpers;
using Hiller.Model.SubModels;
using Hiller.ViewModel.Libs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Hiller.Model
{
    public class Healer : GxNotifyObject
    {

        public static bool Enabled;
        public static Thread Healing = new Thread(HealerVoid);
        public static List<Rule> HealRules = new List<Rule>();

        #region Methods

        public static void SetHealer(bool val)
        {
            Enabled = val;
        }

        public static void HealerVoid()
        {
            while (true)
            {
                if (!Enabled)
                {
                    Thread.Sleep(Randomizer.RandomInt(150, 300));
                    continue;
                }
                if (HealRules.Count == 0)
                {
                    Thread.Sleep(100);
                    continue;
                }
                foreach (Rule rule in HealRules.OrderByDescending(r => r.Priority))
                {
                    //if (CheckRule(rule)) { ExecuteRule(rule); }
                    Thread.Sleep(Randomizer.RandomInt(rule.MinDelay, rule.MaxDelay));
                }
            }
        }

        public static bool CheckRule(Rule rule)
        {
            bool hp_match = false;
            bool mp_match = false;
            switch (rule.HealthType)
            {
                case "E":
                    if (MemoryReader.Health < rule.MinHp) 
                        hp_match = true; 
                    break;
                case "P":
                    if (MemoryReader.HealthPC < rule.MinHp)
                        hp_match = true; 
                    break;
            }
            switch(rule.ManaType)
            {
                case "E":
                    if (MemoryReader.Mana < rule.MinMp)
                        mp_match = true;
                    break;
                case "P":
                    if (MemoryReader.ManaPC < rule.MinMp)
                        mp_match = true;
                    break;
            }
            if (hp_match && mp_match) { return true; }
            return false;
        }

        #endregion

        #region Main



        #endregion
    }
}
