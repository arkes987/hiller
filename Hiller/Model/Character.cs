﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hiller.Model
{
    public class Character
    {
        #region Properties

        public static Process Client { get; set; }

        #endregion

        #region Methods

        public static Process[] GetProcessesFromClassName(string ClassName)
        {
            StringBuilder strBuilder = new StringBuilder(100);
            int classLength = 0;
            List<Process> processList = new List<Process>();
            Process[] processlist = Process.GetProcesses();
            foreach (Process proc in processlist)
            {
                try
                {
                    classLength = Imports.GetClassName(proc.MainWindowHandle, strBuilder, 100);
                    if (strBuilder.ToString() == ClassName)
                    {
                        strBuilder.Remove(0, strBuilder.Length);
                        processList.Add(proc);
                    }
                }
                catch { continue; }
            }
            return processList.ToArray();
        }

        public static List<Process> GetClients()
        {
            return GetProcessesFromClassName("TibiaClient").ToList();
        }

        #endregion
    }
}
