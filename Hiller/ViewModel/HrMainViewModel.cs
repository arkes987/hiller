﻿using Hiller.Model;
using Hiller.ViewModel.Libs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Collections.ObjectModel;
using Hiller.Model.SubModels;
using Hiller.Data;
using Hiller.Helpers;

namespace Hiller.ViewModel
{
    public class HrMainViewModel : GxNotifyObject
    {

        #region .ctor

        public HrMainViewModel()
        {
            RulesCollection = new ObservableCollection<Rule>();
            SpellsCollection = new ObservableCollection<Spell>();
            XmlLoader = new XmlLoader();
            SpellsCollection = XmlLoader.ReadSpells();
            InitInterface();

        }

        #endregion

        #region Properties

        public ObservableCollection<Spell> SpellsCollection { get; set; }


        private ObservableCollection<Rule> _rulesCollection;
        public ObservableCollection<Rule> RulesCollection
        {
            get { return _rulesCollection; }
            set
            {
                if (_rulesCollection != value)
                {
                    _rulesCollection = value;
                    OnPropertyChanged(() => RulesCollection);
                }
            }
        }



        private XmlLoader _xmlLoader;
        public XmlLoader XmlLoader
        {
            get { return _xmlLoader; }
            set
            {
                if (_xmlLoader != value)
                {
                    _xmlLoader = value;
                    OnPropertyChanged(() => XmlLoader);
                }
            }
        }



        private Rule _selectedRule;
        public Rule SelectedRule
        {
            get { return _selectedRule; }
            set
            {
                _selectedRule = value;
                OnPropertyChanged(() => SelectedRule);
            }
        }

        private Spell _selectedSpell;
        public Spell SelectedSpell
        {
            get { return _selectedSpell; }
            set
            {
                if (_selectedSpell != value)
                {
                    _selectedSpell = value;
                    OnPropertyChanged(() => SelectedSpell);
                }
            }
        }

        private bool _healerEnabled;
        public bool HealerEnabled
        {
            get { return _healerEnabled; }
            set
            {
                if (_healerEnabled != value)
                {
                    _healerEnabled = value;
                    OnPropertyChanged(() => HealerEnabled);
                }
            }
        }

        private int _minHp;
        public int MinHp
        {
            get { return _minHp; }
            set
            {
                if (_minHp != value)
                {
                    _minHp = value;
                    OnPropertyChanged(() => MinHp);
                }
            }
        }

        private int _minMp;
        public int MinMp
        {
            get { return _minMp; }
            set
            {
                if (_minMp != value)
                {
                    _minMp = value;
                    OnPropertyChanged(() => MinMp);
                }
            }
        }

        private int _minDelay;
        public int MinDelay
        {
            get { return _minDelay; }
            set
            {
                if (_minDelay != value)
                {
                    _minDelay = value;
                    OnPropertyChanged(() => MinDelay);
                }
            }
        }

        private int _maxDelay;
        public int MaxDelay
        {
            get { return _maxDelay; }
            set
            {
                if (_maxDelay != value)
                {
                    _maxDelay = value;
                    OnPropertyChanged(() => MaxDelay);
                }
            }
        }

        private bool _healthPc;
        public bool HealthPc
        {
            get { return _healthPc; }
            set
            {
                if (_healthPc != value)
                {
                    _healthPc = value;
                    OnPropertyChanged(() => HealthPc);
                }
            }
        }

        private bool _manaPc;
        public bool ManaPc
        {
            get { return _manaPc; }
            set
            {
                if (_manaPc != value)
                {
                    _manaPc = value;
                    OnPropertyChanged(() => ManaPc);
                }
            }
        }

        private int _priority;
        public int Priority
        {
            get { return _priority; }
            set
            {
                if (_priority != value)
                {
                    _priority = value;
                    OnPropertyChanged(() => Priority);
                }
            }
        }

        #endregion

        #region Methods

        public void InitInterface()
        {
            OutputManager.WriteCL("Addresses loaded from source [DONE]", "done");
            HealthPc = true;
            ManaPc = true;
            MinDelay = Randomizer.RandomInt(150, 500);
            MaxDelay = Randomizer.RandomInt(500, 800);
        }

        #endregion


        #region Commands

        #region AddHotkey

        RelayCommand _addHotkeyCommand;
        public ICommand AddHotkeyCommand
        {
            get
            {
                if (_addHotkeyCommand == null)
                {
                    _addHotkeyCommand = new RelayCommand(param => AddHotkeyCommandExecute(param), param => AddHotkeyCommandCanExecute(param));
                }
                return _addHotkeyCommand;
            }
        }
        void AddHotkeyCommandExecute(object param)
        {
            try
            {
                if (SelectedSpell != null)
                {
                    Rule tempRule = new Rule();
                    tempRule.Item = SelectedSpell.SpellName;
                    tempRule.MinHp = MinHp;
                    tempRule.MinMp = MinMp;
                    tempRule.Priority = Priority;

                    #region Value Types
                    if (HealthPc)
                        tempRule.HealthType = "P";
                    else
                        tempRule.HealthType = "E";

                    if (ManaPc)
                        tempRule.ManaType = "P";
                    else
                        tempRule.ManaType = "E";
                    #endregion

                    tempRule.MinDelay = MinDelay;
                    tempRule.MaxDelay = MaxDelay;

                    if (RulesCollection.Count() < 1)
                        RulesCollection.Add(tempRule);
                    else
                        RulesCollection.Insert(Priority, tempRule);
                }
            }
            catch (Exception exc)
            {
                OutputManager.WriteCL(exc);
            }

        }
        bool AddHotkeyCommandCanExecute(object param)
        {
            return true;
        }

        #endregion

        #region EditHotkey

        RelayCommand _editHotkeyCommand;
        public ICommand EditHotkeyCommand
        {
            get
            {
                if (_editHotkeyCommand == null)
                {
                    _editHotkeyCommand = new RelayCommand(param => EditHotkeyCommandExecute(param), param => EditHotkeyCommandCanExecute(param));
                }
                return _editHotkeyCommand;
            }
        }
        void EditHotkeyCommandExecute(object param)
        {
            try
            {

            }
            catch (Exception exc)
            {
                OutputManager.WriteCL(exc);
            }
        }
        bool EditHotkeyCommandCanExecute(object param)
        {
            return true;
        }

        #endregion

        #region RemoveHotkey

        RelayCommand _removeHotkeyCommand;
        public ICommand RemoveHotkeyCommand
        {
            get
            {
                if (_removeHotkeyCommand == null)
                {
                    _removeHotkeyCommand = new RelayCommand(param => RemoveHotkeyCommandExecute(param), param => RemoveHotkeyCommandCanExecute(param));
                }
                return _removeHotkeyCommand;
            }
        }
        void RemoveHotkeyCommandExecute(object param)
        {
            try
            {
                RulesCollection.Remove(SelectedRule);
            }
            catch (Exception exc)
            {
                OutputManager.WriteCL(exc);
            }
        }
        bool RemoveHotkeyCommandCanExecute(object param)
        {
            return true;
        }

        #endregion

        #region HotkeyMinus

        RelayCommand _hotkeyMinusCommand;
        public ICommand HotkeyMinusCommand
        {
            get
            {
                if (_hotkeyMinusCommand == null)
                {
                    _hotkeyMinusCommand = new RelayCommand(param => HotkeyMinusCommandExecute(param), param => HotkeyMinusCommandCanExecute(param));
                }
                return _hotkeyMinusCommand;
            }
        }
        void HotkeyMinusCommandExecute(object param)
        {
            OutputManager.WriteCL<int>(MemoryReader.Health, "done");
            if (SelectedRule != null)
            {
                try
                {
                    int index = SelectedRule.Priority;
                    Rule tempRule = RulesCollection[index];
                    RulesCollection.RemoveAt(index);
                    RulesCollection.Insert(index--, tempRule);
                }
                catch (Exception exc)
                {
                    OutputManager.WriteCL(exc);
                }
            }
        }
        bool HotkeyMinusCommandCanExecute(object param)
        {
            return true;
        }

        #endregion

        #region HotkeyPlus

        RelayCommand _hotkeyPlusCommand;
        public ICommand HotkeyPlusCommand
        {
            get
            {
                if (_hotkeyPlusCommand == null)
                {
                    _hotkeyPlusCommand = new RelayCommand(param => HotkeyPlusCommandExecute(param), param => HotkeyPlusCommandCanExecute(param));
                }
                return _hotkeyPlusCommand;
            }
        }
        void HotkeyPlusCommandExecute(object param)
        {
            if (SelectedRule != null)
            {
                try
                {
                    int index = SelectedRule.Priority;
                    Rule tempRule = RulesCollection[index];
                    RulesCollection.RemoveAt(index);
                    RulesCollection.Insert(index++, tempRule);
                }
                catch (Exception exc)
                {
                    OutputManager.WriteCL(exc);
                }
            }
        }
        bool HotkeyPlusCommandCanExecute(object param)
        {
            return true;
        }

        #endregion

        #endregion

    }
}
