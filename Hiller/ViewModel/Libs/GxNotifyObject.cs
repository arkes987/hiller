﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Hiller.ViewModel.Libs
{
    public class GxNotifyObject : INotifyPropertyChanged, IDisposable
    {
        #region Event

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion Event

        #region Methods

        public void Dispose()
        {
            if (PropertyChanged != null)
            {
                var delgates = PropertyChanged.GetInvocationList().ToList();
                foreach (var del in delgates)
                {
                    PropertyChanged -= (PropertyChangedEventHandler)del;
                }
            }

            IsDisposed = true;
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            try
            {
                PropertyChangedEventHandler handler = System.Threading.Interlocked.CompareExchange(ref PropertyChanged, null, null);
                if (handler != null)
                    handler(this, new PropertyChangedEventArgs(propertyName));

            }
            catch (Exception ex)
            {

            }
        }

        protected virtual void OnPropertyChanged(Expression<Func<object>> extension)
        {
            try
            {
                PropertyChangedEventHandler handler = System.Threading.Interlocked.CompareExchange(ref PropertyChanged, null, null);
                if (handler != null)
                    handler(this, new PropertyChangedEventArgs(extension.GetPropertyName()));

            }
            catch (Exception ex)
            {

            }
        }

        #endregion Methods

        #region Properties

        public bool IsDisposed { get; set; }

        #endregion Properties
    }
}
