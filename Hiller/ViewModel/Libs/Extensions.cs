﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Hiller.ViewModel.Libs
{
    public static class Extensions
    {
        public static string GetPropertyName(this Expression<Func<object>> extension)
        {
            UnaryExpression unaryExpression = extension.Body as UnaryExpression;
            MemberExpression memberExpression = unaryExpression != null ?
                (MemberExpression)unaryExpression.Operand :
                (MemberExpression)extension.Body;

            return memberExpression.Member.Name;
        }
    }
}
