﻿using Hiller.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Hiller.ViewModel.Libs;
using System.Windows;
using Hiller.View;
using Patcher.Model;

namespace Hiller.ViewModel
{
    public class HrLoginViewModel : GxNotifyObject
    {



        #region .ctor

        public HrLoginViewModel()
        {
            Character = new Model.Character();
            Characters = Character.GetClients();
            Main = new HrMainView();
        }

        #endregion



        #region Properties

        private List<Process> _characters;
        public List<Process> Characters
        {
            get { return _characters; }
            set
            {
                if (_characters != value)
                {
                    _characters = value;
                    OnPropertyChanged(() => Characters);
                }
            }
        }


        

        private Process _selectedClient;
        public Process SelectedClient
        {
            get { return _selectedClient; }
            set
            {
                if (_selectedClient != value)
                {
                    _selectedClient = value;
                    OnPropertyChanged(() => SelectedClient);
                }
            }
        }



        private Character _character;
        public Character Character
        {
            get { return _character; }
            set
            {
                if (_character != value)
                    _character = value;
            }
        }

        private HrMainView _main;
        public HrMainView Main
        {
            get { return _main; }
            set
            {
                if (_main != value)
                {
                    _main = value;
                    OnPropertyChanged(() => Main);
                }
            }
        }

        private Core _core;
        public Core Core
        {
            get { return _core; }
            set
            {
                if (_core != value)
                {
                    _core = value;
                    OnPropertyChanged(() => Core);
                }
            }
        }
        
        
        



        #endregion

        #region Commands

        #region Launch

        RelayCommand _launchCommand;
        public ICommand LaunchCommand
        {
            get
            {
                if (_launchCommand == null)
                {
                    _launchCommand = new RelayCommand(param => LaunchCommandExecute(param), param => LaunchCommandCanExecute(param));
                }
                return _launchCommand;
            }
        }
        void LaunchCommandExecute(object param)
        {
            Character.Client = SelectedClient;
            Main.Show();
            Core = new Core(Character.Client);
            Core.StartSearch();
            
        }
        bool LaunchCommandCanExecute(object param)
        {
            return true;
        }

        #endregion

        #region Refresh

        RelayCommand _refreshCommand;
        public ICommand RefreshCommand
        {
            get
            {
                if (_refreshCommand == null)
                {
                    _refreshCommand = new RelayCommand(param => RefreshCommandExecute(param), param => RefreshCommandCanExecute(param));
                }
                return _refreshCommand;
            }
        }
        void RefreshCommandExecute(object param)
        {
            if (Characters.Count > 0)
                Characters.Clear();

            Characters = Character.GetClients();
        }
        bool RefreshCommandCanExecute(object param)
        {
            return true;
        }

        #endregion

        #endregion

    }
}
